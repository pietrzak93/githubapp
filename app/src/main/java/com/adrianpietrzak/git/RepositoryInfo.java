package com.adrianpietrzak.git;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adrian on 2015-12-17.
 */
public class RepositoryInfo {
    private String name;
    private String eventUrl;
    private List<IssueInfo> listIssues;

    public RepositoryInfo(String name, String eventUrl) {
        this.name = name;
        this.eventUrl = eventUrl;
        this.listIssues = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEventUrl() {
        return eventUrl;
    }

    public void setEventUrl(String eventUrl) {
        this.eventUrl = eventUrl;
    }

    public List<IssueInfo> getListIssues() {
        return listIssues;
    }

    public void setListIssues(List<IssueInfo> listIssues) {
        this.listIssues = listIssues;
    }

    @Override
    public String toString() {
        return "RepositoryInfo{" +
                "name='" + name + '\'' +
                ", eventUrl='" + eventUrl + '\'' +
                ", listIssues=" + listIssues +
                '}';
    }
}



