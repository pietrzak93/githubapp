package com.adrianpietrzak.git.helpers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Adrian on 2015-12-18.
 */
public class Tools {

    /**
     * @return
     * true if net is available
     * false otherwise
     */
    public static Boolean isNetAvailable(final Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager)
                    context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo wifiInfo = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            NetworkInfo mobileInfo =
                    connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
            if (wifiInfo != null && wifiInfo.isConnected() ||
                    mobileInfo != null && mobileInfo.isConnected()) {
                return true;
            }
        } catch (Exception ignored) {

        }

        return false;
    }
}
