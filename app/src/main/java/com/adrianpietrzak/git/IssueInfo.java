package com.adrianpietrzak.git;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Adrian on 2015-12-17.
 */
public class IssueInfo {
    private int number;
    private String tittle;
    private String status;
    private String assigneeUser;
    private String body;
    private String commentsUrl;
    private List<CommentInfo> listComments;

    public IssueInfo(int number, String tittle, String status, String assigneeUser, String body, String commentsUrl) {
        this.number = number;
        this.tittle = tittle;
        this.status = status;
        this.assigneeUser = assigneeUser;
        this.body = body;
        this.commentsUrl = commentsUrl;
        this.listComments = new ArrayList<>();
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTittle() {
        return tittle;
    }

    public void setTittle(String tittle) {
        this.tittle = tittle;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAssigneeUser() {
        return assigneeUser;
    }

    public void setAssigneeUser(String assigneeUser) {
        this.assigneeUser = assigneeUser;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getCommentsUrl() {
        return commentsUrl;
    }

    public void setCommentsUrl(String commentsUrl) {
        this.commentsUrl = commentsUrl;
    }

    public List<CommentInfo> getListComments() {
        return listComments;
    }

    public void setListComments(List<CommentInfo> listComments) {
        this.listComments = listComments;
    }

    @Override
    public String toString() {
        return "IssueInfo{" +
                "number=" + number +
                ", tittle='" + tittle + '\'' +
                ", status='" + status + '\'' +
                ", assigneeUser='" + assigneeUser + '\'' +
                ", body='" + body + '\'' +
                ", commentsUrl='" + commentsUrl + '\'' +
                ", listComments=" + listComments +
                '}';
    }
}



