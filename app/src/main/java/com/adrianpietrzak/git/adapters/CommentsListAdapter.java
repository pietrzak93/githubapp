package com.adrianpietrzak.git.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.adrianpietrzak.git.CommentInfo;
import com.adrianpietrzak.git.R;
import java.util.List;

public class CommentsListAdapter extends ArrayAdapter<CommentInfo> {
    private Activity mContext;

    static class ViewHolder {
        public TextView textViewUserLogin;
        public TextView textViewComment;
    }

    public CommentsListAdapter(Activity context, List<CommentInfo> listIssues) {
        super(context, R.layout.adapter_layout_comments, listIssues);
        this.mContext = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = mContext.getLayoutInflater();
        convertView = inflater.inflate(R.layout.adapter_layout_comments, null);
        final ViewHolder viewHolder = new ViewHolder();
        viewHolder.textViewUserLogin = (TextView) convertView.findViewById(R.id.textViewUserLogin);
        viewHolder.textViewComment = (TextView) convertView.findViewById(R.id.textViewComment);
        convertView.setTag(viewHolder);

        final CommentInfo commentInfo = getItem(position);
        viewHolder.textViewUserLogin.setText(commentInfo.getUserLogin());
        viewHolder.textViewComment.setText(commentInfo.getBody());

        return convertView;
    }
}