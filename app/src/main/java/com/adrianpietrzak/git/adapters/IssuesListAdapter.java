package com.adrianpietrzak.git.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.adrianpietrzak.git.IssueInfo;
import com.adrianpietrzak.git.R;
import java.util.List;

public class IssuesListAdapter extends ArrayAdapter<IssueInfo> {
    private Activity mContext;

    static class ViewHolder {
        public TextView textViewName;
        public TextView textViewStatus;
        public TextView textViewAssignee;
    }

    public IssuesListAdapter(Activity context, List<IssueInfo> listIssues) {
        super(context, R.layout.adapter_layout_issues, listIssues);
        this.mContext = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = mContext.getLayoutInflater();
        convertView = inflater.inflate(R.layout.adapter_layout_issues, null);
        final ViewHolder viewHolder = new ViewHolder();
        viewHolder.textViewName = (TextView) convertView.findViewById(R.id.textViewName);
        viewHolder.textViewStatus = (TextView) convertView.findViewById(R.id.textViewStatus);
        viewHolder.textViewAssignee = (TextView) convertView.findViewById(R.id.textViewAssignee);
        convertView.setTag(viewHolder);

        final IssueInfo issueInfo = getItem(position);
        viewHolder.textViewName.setText(issueInfo.getTittle());
        viewHolder.textViewStatus.setText(issueInfo.getStatus());

        String assigneeUser = issueInfo.getAssigneeUser();

        if (assigneeUser.equals("null")) {
            assigneeUser = mContext.getString(R.string.no_one_assignee);
        }
        viewHolder.textViewAssignee.setText(assigneeUser);

        return convertView;
    }
}