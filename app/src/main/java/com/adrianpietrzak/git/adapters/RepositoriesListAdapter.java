package com.adrianpietrzak.git.adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import com.adrianpietrzak.git.R;
import com.adrianpietrzak.git.RepositoryInfo;
import java.util.List;

public class RepositoriesListAdapter extends ArrayAdapter<RepositoryInfo> {
    private Activity mContext;

    static class ViewHolder {
        public TextView textViewName;
    }

    public RepositoriesListAdapter(Activity context, List<RepositoryInfo> listRepositories) {
        super(context, R.layout.adapter_layout_repositories, listRepositories);
        this.mContext = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = mContext.getLayoutInflater();
        convertView = inflater.inflate(R.layout.adapter_layout_repositories, null);
        final ViewHolder viewHolder = new ViewHolder();
        viewHolder.textViewName = (TextView) convertView.findViewById(R.id.textViewName);
        convertView.setTag(viewHolder);

        final RepositoryInfo repositoryInfo = getItem(position);
        viewHolder.textViewName.setText(repositoryInfo.getName());

        return convertView;
    }
}