package com.adrianpietrzak.git;

/**
 * Created by Adrian on 2015-12-17.
 */
public class CommentInfo {
    private String userLogin;
    private String body;

    public CommentInfo(String userLogin, String body) {
        this.userLogin = userLogin;
        this.body = body;
    }

    public String getUserLogin() {
        return userLogin;
    }

    public void setUserLogin(String userLogin) {
        this.userLogin = userLogin;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "CommentInfo{" +
                "userLogin='" + userLogin + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}



