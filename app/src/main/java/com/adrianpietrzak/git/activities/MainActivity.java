package com.adrianpietrzak.git.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.adrianpietrzak.git.CommentInfo;
import com.adrianpietrzak.git.IssueInfo;
import com.adrianpietrzak.git.R;
import com.adrianpietrzak.git.RepositoryInfo;
import com.adrianpietrzak.git.adapters.CommentsListAdapter;
import com.adrianpietrzak.git.adapters.IssuesListAdapter;
import com.adrianpietrzak.git.adapters.RepositoriesListAdapter;
import com.adrianpietrzak.git.helpers.Tools;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends Activity {
    private final int STATE_LOGGED_OUT = 0;
    private final int STATE_REPO_LIST = 1;
    private final int STATE_ISSUE_LIST = 2;
    private final int STATE_ISSUE_DETAIL = 3;

    @Bind(R.id.layoutSignIn)
    RelativeLayout layoutSignIn;
    @Bind(R.id.editTextLogin)
    EditText editTextLogin;
    @Bind(R.id.editTextPassword)
    EditText editTextPassword;

    @Bind(R.id.textViewInformation)
    TextView textViewInformation;
    @Bind(R.id.textViewDescription)
    TextView textViewDescription;
    @Bind(R.id.textViewDescriptionInfo)
    TextView textViewDescriptionInfo;
    @Bind(R.id.textViewNoComments)
    TextView textViewNoComments;
    @Bind(R.id.listViewRepositories)
    ListView listView;

    private Context mContext;
    private Activity mActivity;

    private List<RepositoryInfo> listRepositories;

    private int mState = STATE_LOGGED_OUT;
    private int selectedRepo;
    private int selectedIssue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mContext = this;
        mActivity = MainActivity.this;

        initialize();
    }

    private void initialize() {
        listRepositories = new ArrayList<>();
    }

    @OnClick(R.id.buttonSignIn)
    void buttonSignIn() {
        if (Tools.isNetAvailable(mContext)) {
            String login = editTextLogin.getText().toString();
            String password = editTextPassword.getText().toString();

            if (login.isEmpty()) {
                Toast.makeText(mContext, getString(R.string.error_no_login), Toast.LENGTH_LONG).show();

                return;
            }

            if (password.isEmpty()) {
                Toast.makeText(mContext, getString(R.string.error_no_password), Toast.LENGTH_LONG).show();

                return;
            }

            new DownloadDataFromNet().execute(login, password);
        } else {
            Toast.makeText(mContext, getString(R.string.error_internet_connection), Toast.LENGTH_LONG).show();
        }
    }

    private void changeState(int newState) {
        mState = newState;

        layoutSignIn.setVisibility(View.GONE);
        listView.setVisibility(View.GONE);
        textViewInformation.setVisibility(View.GONE);
        textViewDescription.setVisibility(View.GONE);
        textViewDescriptionInfo.setVisibility(View.GONE);
        textViewNoComments.setVisibility(View.GONE);

        switch (newState) {
            case STATE_LOGGED_OUT:
                listRepositories.clear();
                layoutSignIn.setVisibility(View.VISIBLE);
                break;
            case STATE_REPO_LIST:
                showRepoList();
                break;
            case STATE_ISSUE_LIST:
                showIssueList();
                break;
            case STATE_ISSUE_DETAIL:
                showIssueDetails();
                break;
        }
    }

    private void showRepoList() {
        listView.setVisibility(View.VISIBLE);
        textViewInformation.setVisibility(View.VISIBLE);
        textViewInformation.setText(getString(R.string.repository_list));

        if (listRepositories.size() > 1) {
            RepositoriesListAdapter repositoriesListAdapter = new RepositoriesListAdapter(mActivity, listRepositories);
            listView.setAdapter(repositoriesListAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selectedRepo = position;
                    changeState(STATE_ISSUE_LIST);
                }
            });
        } else if (listRepositories.size() == 1) {
            selectedRepo = 0;
            changeState(STATE_ISSUE_LIST);
        } else {
            listView.setVisibility(View.GONE);
            textViewNoComments.setVisibility(View.VISIBLE);
            textViewNoComments.setText(getString(R.string.there_is_no_repositories));
        }
    }

    private void showIssueList() {
        listView.setVisibility(View.VISIBLE);
        textViewInformation.setVisibility(View.VISIBLE);
        textViewInformation.setText(getString(R.string.issues_list));

        if (!listRepositories.get(selectedRepo).getListIssues().isEmpty()) {
            IssuesListAdapter issuesListAdapter = new IssuesListAdapter(mActivity,
                    listRepositories.get(selectedRepo).getListIssues());
            listView.setAdapter(issuesListAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selectedIssue = position;
                    changeState(STATE_ISSUE_DETAIL);
                }
            });
        } else {
            listView.setVisibility(View.GONE);
            textViewNoComments.setVisibility(View.VISIBLE);
            textViewNoComments.setText(getString(R.string.there_is_no_issues));
        }
    }

    private void showIssueDetails() {
        listView.setVisibility(View.VISIBLE);
        textViewInformation.setVisibility(View.VISIBLE);
        textViewInformation.setText(getString(R.string.comments_list));
        textViewDescription.setVisibility(View.VISIBLE);

        String description = listRepositories.get(selectedRepo).getListIssues().get(selectedIssue).getBody();
        if (description.isEmpty()) {
            description = getString(R.string.there_is_no_description);
        }
        textViewDescription.setText(description);
        textViewDescriptionInfo.setVisibility(View.VISIBLE);

        if (!listRepositories.get(selectedRepo).getListIssues().get(selectedIssue).getListComments().isEmpty()) {
            CommentsListAdapter commentsListAdapter = new CommentsListAdapter(mActivity,
                    listRepositories.get(selectedRepo).getListIssues().get(selectedIssue).getListComments());
            listView.setAdapter(commentsListAdapter);
        } else {
            listView.setVisibility(View.GONE);
            textViewNoComments.setVisibility(View.VISIBLE);
            textViewNoComments.setText(getString(R.string.there_is_no_comments_yet));
        }
    }

    private class DownloadDataFromNet extends AsyncTask<String, Boolean, Boolean> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            progressDialog = new ProgressDialog(mContext);
            progressDialog.setIndeterminate(true);
            progressDialog.setCancelable(false);
            progressDialog.setTitle(getString(R.string.connecting));
            progressDialog.show();
        }

        @Override
        protected Boolean doInBackground(String... params) {
            String login = params[0];
            String password = params[1];

            return getUserReposList(login, password);
        }

        @Override
        protected void onPostExecute(Boolean isTrue) {
            super.onPostExecute(isTrue);

            progressDialog.dismiss();

            if (!isTrue) {
                Toast.makeText(mContext, getString(R.string.error_bad_password_or_login), Toast.LENGTH_LONG).show();
            } else {
                changeState(STATE_REPO_LIST);
            }

            editTextLogin.setText("");
            editTextPassword.setText("");
        }
    }

    /*
     * Return true if success otherwise false
     */
    private boolean getUserReposList(String login, String password) {
        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet("https://api.github.com/user/repos");

        httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(login, password), "UTF-8", false));
        httpGet.setHeader("Content-Type", "application/json");
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }

                JSONArray jsonArray = new JSONArray(builder.toString());

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    RepositoryInfo repositoryInfo = new RepositoryInfo(
                            jsonObject.getString("name"), jsonObject.getString("events_url"));

                    getRepoIssues(login, password, repositoryInfo);
                    listRepositories.add(repositoryInfo);
                }

                return true;
            } else {
                return false;

            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

        return false;
    }

    private void getRepoIssues(String login, String password, RepositoryInfo repositoryInfo) {
        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(repositoryInfo.getEventUrl());
        httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(login, password), "UTF-8", false));
        httpGet.setHeader("Content-Type", "application/json");
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }

                JSONArray jsonArray = new JSONArray(builder.toString());

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);

                    String type = jsonObject.getString("type");
                    if (type.equals("IssuesEvent")) {
                        JSONObject jsonPayload = jsonObject.getJSONObject("payload");
                        JSONObject jsonIssue = jsonPayload.getJSONObject("issue");
                        String loginAssignee = "null";
                        if (!jsonIssue.getString("assignee").equals("null")) {
                            JSONObject jsonAssignee = jsonIssue.getJSONObject("assignee");
                            loginAssignee = jsonAssignee.getString("login");
                        }

                        int number = jsonIssue.getInt("number");

                        if (!checkIfIssueIsOnList(repositoryInfo.getListIssues(), number)) {
                            IssueInfo issueInfo = new IssueInfo(number,
                                    jsonIssue.getString("title"),
                                    jsonIssue.getString("state"),
                                    loginAssignee,
                                    jsonIssue.getString("body"),
                                    jsonIssue.getString("comments_url"));

                            getIssueComments(login, password, issueInfo);

                            repositoryInfo.getListIssues().add(issueInfo);
                        }
                    }
                }
            } else {
                Log.e("DEBUG", "Failed to download file");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    private void getIssueComments(String login, String password, IssueInfo issueInfo) {
        StringBuilder builder = new StringBuilder();
        HttpClient client = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(issueInfo.getCommentsUrl());
        httpGet.addHeader(BasicScheme.authenticate(new UsernamePasswordCredentials(login, password), "UTF-8", false));
        httpGet.setHeader("Content-Type", "application/json");
        try {
            HttpResponse response = client.execute(httpGet);
            StatusLine statusLine = response.getStatusLine();
            int statusCode = statusLine.getStatusCode();
            if (statusCode == 200) {
                HttpEntity entity = response.getEntity();
                InputStream content = entity.getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(content));
                String line;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                }

                JSONArray jsonArray = new JSONArray(builder.toString());

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonComment = jsonArray.getJSONObject(i);
                    JSONObject jsonUser = jsonComment.getJSONObject("user");

                    issueInfo.getListComments().add(new CommentInfo(jsonUser.getString("login"), jsonComment.getString("body")));
                }
            } else {
                Log.e("DEBUG", "Failed to download file");
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean checkIfIssueIsOnList(List<IssueInfo> listIssues, int number) {
        for (IssueInfo issueInfo : listIssues) {
            if (number == issueInfo.getNumber()) {
                return true;
            }
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        switch (mState) {
            case STATE_LOGGED_OUT:
                this.finish();
                break;
            case STATE_REPO_LIST:
                changeState(STATE_LOGGED_OUT);
                break;
            case STATE_ISSUE_LIST:
                if (listRepositories.size() == 1) {
                    changeState(STATE_LOGGED_OUT);
                } else {
                    changeState(STATE_REPO_LIST);
                }
                break;
            case STATE_ISSUE_DETAIL:
                changeState(STATE_ISSUE_LIST);
                break;
        }
    }
}
